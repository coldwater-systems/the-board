# frozen_string_literal: true

# if User.empty?
  user = User.new(username: 'admin', display_name: 'Administrator', password: 'admin',
                  can_invite: true, moderator: true, operator: true, password_expired: true)
  user.save!(validate: false) # Intentionally weak password
# end
