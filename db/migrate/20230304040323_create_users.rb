# frozen_string_literal: true

class CreateUsers < ActiveRecord::Migration[7.0]
  def change
    create_table :users do |t|
      t.string :username, null: false, index: {unique: true}
      t.string :display_name, null: false
      t.string :password_digest, null: false
      t.boolean :password_expired, default: false, null: false
      t.timestamps

      t.references :invited_by, foreign_key: {to_table: 'users'}

      # Grant flags
      t.boolean :banned, default: false, null: false
      t.boolean :can_invite, default: false, null: false
      t.boolean :moderator, default: false, null: false
      t.boolean :operator, default: false, null: false
    end
  end
end
