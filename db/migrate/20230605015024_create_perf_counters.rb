# frozen_string_literal: true

class CreatePerfCounters < ActiveRecord::Migration[7.0]
  def change
    create_table :perf_counters do |t|
      t.string :name, null: false, index: true
      t.integer :value, null: false

      t.timestamps
    end
  end
end
