# frozen_string_literal: true

class CreateInvites < ActiveRecord::Migration[7.0]
  def change
    create_table :invites do |t|
      t.string :code, null: false
      t.datetime :expires, null: false
      t.timestamps

      t.references :created_by, null: false, foreign_key: {to_table: 'users'}
      t.references :claimed_by, foreign_key: {to_table: 'users'}
    end
  end
end
