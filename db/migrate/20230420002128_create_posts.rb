# frozen_string_literal: true

class CreatePosts < ActiveRecord::Migration[7.0]
  def change
    create_table :posts do |t|
      t.belongs_to :owner, null: false, foreign_key: {to_table: 'users'}
      t.boolean :published, null: false, default: false

      t.timestamps
    end

    create_table :post_revisions do |t|
      t.references :post, null: false, foreign_key: true
      # t.rich_text :body # not actually stored in this table (See https://edgeguides.rubyonrails.org/action_text_overview.html#creating-rich-text-content)
      t.references :author, null: false, foreign_key: {to_table: 'users'}

      t.timestamps
    end
  end
end
