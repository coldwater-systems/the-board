# frozen_string_literal: true

class AlterPostsPublished < ActiveRecord::Migration[7.0]
  def change
    remove_column :posts, :published, :boolean, null: false, default: false
    add_reference :posts, :published, foreign_key: {to_table: 'post_revisions'}
    add_column :posts, :published_at, :datetime

    reversible do |dir|
      dir.up do
        Post.all.each do |post|
          post.published = post.revisions.last
          post.published_at = post.created_at
          post.save!
        end
      end
    end
  end
end
