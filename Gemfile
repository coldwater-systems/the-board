# frozen_string_literal: true

source 'https://rubygems.org'

ruby '~> 3.2'

# Not present in any docker image
group :development do
  gem 'debug', platforms: [:mri, :mingw, :x64_mingw]
  gem 'pry-rails'
end

# These gems are temporarily installed during the docker build
group :build do
  gem 'brakeman'
  gem 'bundler-audit'
  gem 'rspec-rails', '~> 6.0'
  gem 'sassc-rails', '~> 2.1'
end

# The following gems are the only ones that appear in the final docker image
gem 'base32', '~> 0.3'
gem 'bcrypt', '~> 3.1'
gem 'bootsnap', '~> 1.16', require: false # required in config/boot.rb
gem 'grape', '~> 1.7'
gem 'haml-rails', '~> 2.0'
gem 'importmap-rails', '~> 1.1'
gem 'jbuilder', '~> 2.11'
gem 'puma', '~> 6.0'
gem 'rails', '~> 7.0'
gem 'ruby-xz', '~> 1.0', require: 'xz'
gem 'sprockets-rails', '~> 3.4'
gem 'sqlite3', '~> 1.4'
gem 'stimulus-rails', '~> 1.2'
gem 'turbo-rails', '~> 1.3'
gem 'tzinfo-data', platforms: [:mingw, :mswin, :x64_mingw, :jruby]
