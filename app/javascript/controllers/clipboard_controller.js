import { Controller } from "@hotwired/stimulus"

export default class extends Controller {
  copy_value(event) {
    const value = event.params.value
    navigator.clipboard.writeText(value).then(() => {
      alert("Link copied to clipboard: " + value)
    })
  }
}
