import { Controller } from "@hotwired/stimulus"

export default class extends Controller {
  expand(event) {
    const button = event.target
    button.dataset.action = 'comments#collapse'
    button.textContent = button.dataset.discardText
    button.classList.remove('new-message')
    button.classList.add('outline', 'trash')

    const expander = document.getElementById(button.dataset.expanderId)
    expander.classList.add('expanded')
  }

  collapse(event) {
    const button = event.target
    button.dataset.action = 'comments#expand'
    button.textContent = button.dataset.expandText
    button.classList.remove('outline', 'trash')
    button.classList.add('new-message')

    const expander = document.getElementById(button.dataset.expanderId)
    expander.classList.remove('expanded')
  }
}
