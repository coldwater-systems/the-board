# frozen_string_literal: true

module TheBoard
  class ApiV1 < Grape::API
    format :json

    get do
      {
        application: 'coldwater.systems:the-board',
        version:     Manifest.version,
        commit_sha:  Manifest.commit_sha,
        _links:      {
          source_code: JsonLink.to(Manifest.source_code_url),
          issues:      JsonLink.to(Manifest.issues_url),
        }.compact,
      }
    end
  end
end
