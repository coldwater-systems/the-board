# frozen_string_literal: true

module CommentsHelper
  def new_comment_button(expand_text, discard_text, expander_id:, class: nil)
    button_tag(expand_text, class:, name: nil, type: nil, data: {
      controller:   'comments',
      action:       'comments#expand',
      expander_id:,
      expand_text:,
      discard_text:,
    })
  end
end
