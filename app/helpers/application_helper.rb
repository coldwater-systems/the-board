# frozen_string_literal: true

module ApplicationHelper
  # Use to turn on or off CSS classes for given condition(s)
  def class?(**kwargs)
    kwargs.filter_map do |key, condition|
      key.to_s.gsub('_', '-') if condition
    end.join(' ')
  end

  def copy_to_clipboard_button(name, value, class: nil, title: 'Copy')
    button_tag(name, name: nil, type: nil, class:, title:, data: {
      controller:            'clipboard',
      action:                'clipboard#copy_value',
      clipboard_value_param: value,
    })
  end

  # See turbo-rails app/controllers/turbo/frames/frame_request.rb:29
  def turbo_frame_request?
    request.headers['Turbo-Frame'].present?
  end
end

class Array
  def to_human(combinator)
    case length
    when 0 then ''
    when 1 then first
    when 2 then "#{first} #{combinator} #{last}"
    else "#{self[...-1].join(', ')}, #{combinator} #{last}"
    end
  end
end
