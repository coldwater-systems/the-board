# frozen_string_literal: true

module PostsHelper
  def infinite_scroll_container(before_id:, &)
    if before_id
      turbo_frame_tag("infinite-scroll-#{before_id}", target: '_top', &)
    else
      yield
      nil
    end
  end

  def infinite_scroll_next(before_id:, &)
    turbo_frame_tag("infinite-scroll-#{before_id}",
                    src:     root_path(before_id:),
                    target:  '_top',
                    loading: 'lazy', &)
  end
end
