# frozen_string_literal: true

module SettingsHelper
  def new_invite_button(name)
    button_tag(name, name: nil, type: nil, data: {
      controller: 'invite',
      action:     'invite#new',
    })
  end

  def sections_bottom(sections_visible, height:)
    visible_sections = sections_visible.select {|_, v| v }
    visible_sections.each_with_index.to_h do |(k, _), i|
      [k, (visible_sections.length - i - 1) * height]
    end
  end
end
