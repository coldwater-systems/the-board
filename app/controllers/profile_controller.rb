# frozen_string_literal: true

class ProfileController < ApplicationController
  skip_before_action :authenticate_session, only: [:accept_invite, :new, :create]
  before_action :set_invite_from_session, only: [:new, :create]

  # GET /invite/:code
  def accept_invite
    @invite = Invite.find_by!(code: params[:code], claimed_by: nil)

    # TODO: it might be better to not 404 and give an explanatory error message
    raise ActiveRecord::RecordNotFound if @invite.expires < Time.now

    reset_session
    session[:invite_id] = @invite.id
    session[:auth_iss] = Time.now.tv_sec
    session[:auth_exp] = (Time.now + Rails.configuration.x.session_expire_after).tv_sec
    redirect_to new_profile_url
  end

  # GET /profile/new
  def new
    @new_user = User.new
    @brand_path = nil # Don't let the user accidentally navigate to the login page
  end

  # GET /profile/edit within <turbo-frame id="profile_form">
  def edit
    @invite_count = Invite.where(created_by_id: @authenticated_user.id).where.not(claimed_by_id: nil).count
  end

  # POST /profile
  def create
    @new_user = User.new(new_user_params)
    @new_user.invited_by = @invite.created_by

    Invite.transaction do
      if @new_user.save
        @invite.claimed_by = @new_user
        @invite.save!
        reset_session
        session[:user_id] = @new_user.id
        session[:auth_iss] = Time.now.tv_sec
        session[:auth_exp] = (Time.now + Rails.configuration.x.session_expire_after).tv_sec
        redirect_to root_url
      else
        render :new
      end
    end
  end

  # PATCH /profile within <turbo-frame id="profile_form">
  def update
    if @authenticated_user.update(update_params)
      redirect_to_user
    else
      render :edit
    end
  end

  # PATCH /profile/username
  def update_username
    @success = @authenticated_user.update(update_username_params)
  end

  # GET /profile
  def redirect_to_user
    redirect_to user_url(@authenticated_user.username)
  end

  private

  def set_invite_from_session
    @invite = Invite.find(session[:invite_id])
  end

  def new_user_params
    params.require(:user).permit(:username, :display_name, :password, :password_confirmation)
  end

  def update_params
    params.require(:user).permit(:display_name)
  end

  def update_username_params
    params.require(:user).permit(:username, :display_name)
  end
end
