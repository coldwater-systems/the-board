# frozen_string_literal: true

class SessionController < ApplicationController
  skip_before_action :authenticate_session, only: [:login, :check_login]

  # GET /login
  def login
    redirect_to root_url if session.key?(:user_id)
  end

  # POST /login
  def check_login
    username, password = params.require(:user).values_at(:username, :password)
    validation_error = nil

    if username.empty?
      validation_error = I18n.t('session.missing_username')
    elsif password.empty?
      validation_error = I18n.t('session.missing_password')
    else
      @authenticated_user = User.find_and_authenticate(username, password)
      if !@authenticated_user
        validation_error = I18n.t('session.bad_credentials')
      elsif @authenticated_user.banned?
        validation_error = I18n.t('session.account_disabled')
      else
        session_setup
        redirect_to root_url
        return
      end
    end
    flash[:notice] = validation_error
    flash[:username] = username
    redirect_to login_url
  end

  # PATCH /password
  def update_password
    if @authenticated_user.authenticate(params[:user][:old_password])
      @success = @authenticated_user.update(settings_pwchange_params)
    else
      @success = false
      @authenticated_user.errors.add(:old_password, I18n.t('errors.messages.incorrect'))
    end
  end

  # POST /logout
  def logout
    reset_session
    redirect_to login_url
  end

  private

  # TODO: centralize this logic
  def session_setup
    reset_session
    session[:user_id] = @authenticated_user.id
    session[:auth_iss] = Time.now.tv_sec
    session[:auth_exp] = (Time.now + Rails.configuration.x.session_expire_after).tv_sec
  end

  def settings_pwchange_params
    params.require(:user).permit(:password, :password_confirmation)
  end
end
