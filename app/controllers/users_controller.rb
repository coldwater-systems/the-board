# frozen_string_literal: true

class UsersController < ApplicationController
  before_action :authenticated_user_must_be_operator, only: [:ban, :unban]
  before_action :set_user, only: [:show, :ban, :unban]

  # GET /users/:id
  def show
    @invite_count = Invite.where(created_by: @user).where.not(claimed_by: nil).count
    @posts = Post.where(owner: @user).order(created_at: :desc)
    @posts = @posts.where.not(published: nil) if @user != @authenticated_user
  end

  def ban
    @user.banned = true
    @user.save!
    redirect_to @user, notice: I18n.t('users.banned')
  end

  def unban
    @user.banned = false
    @user.save!
    redirect_to @user, notice: I18n.t('users.unbanned')
  end

  private

  def set_user
    @user = User.find_by!(username: params[:username])
  end

  def authenticated_user_must_be_operator
    raise ForbiddenAndInvisible unless @authenticated_user.operator?
  end
end
