# frozen_string_literal: true

class SettingsController < ApplicationController
  # GET /settings
  def show
    @unclaimed_invites = @authenticated_user.created_invites.where(claimed_by: nil)
    @claimed_invites = @authenticated_user.created_invites.where.not(claimed_by: nil)
    @sections_visible = {}
    @themes = Theme.all
  end
end
