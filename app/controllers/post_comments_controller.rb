# frozen_string_literal: true

class PostCommentsController < ApplicationController
  # GET /posts/1/comments
  def redirect_to_post
    redirect_to post_url(params[:id])
  end

  # POST /posts/1/comments
  def create
    PostComment.create!(author: @authenticated_user, post_id: params[:id], **post_comment_params)
    redirect_to post_url(params[:id])
  end

  private

  def post_comment_params
    params.require(:post_comment).permit(:text)
  end
end
