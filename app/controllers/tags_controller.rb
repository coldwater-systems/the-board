# frozen_string_literal: true

class TagsController < ApplicationController
  before_action :user_must_be_operator
  before_action :set_tag, only: [:destroy]

  # GET /tags
  def index
    @tags = Tag.all
  end

  # GET /tags/new
  def new
    @tag = Tag.new
  end

  # POST /tags
  def create
    @tag = Tag.new(tag_params)

    if @tag.save
      redirect_to tags_url
    else
      render :new, status: :unprocessable_entity
    end
  end

  # DELETE /tags/:slug
  def destroy
    @tag.destroy
    redirect_to tags_url
  end

  private

  def user_must_be_operator
    raise ForbiddenAndInvisible unless @authenticated_user.operator?
  end

  def set_tag
    @tag = Tag.find_by!(slug: params[:slug])
  end

  def tag_params
    params.require(:tag).permit(:name)
  end
end
