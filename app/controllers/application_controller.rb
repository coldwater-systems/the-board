# frozen_string_literal: true

class ApplicationController < ActionController::Base
  before_action :track_visit
  before_action :purge_expired_session
  before_action :set_authenticated_user
  before_action :authenticate_session
  before_action :set_theme
  before_action :set_brand_path

  class ForbiddenAndInvisible < StandardError; end
  class Forbidden < StandardError; end

  private

  def track_visit
    PerfCounter.hit("route:#{request.method} #{request.path}")
  end

  def purge_expired_session
    # Don't purge anonymous sessions
    return unless session.key?(:user_id) || session.key?(:invite_id)

    reset_session if !session.key?(:auth_exp) || session[:auth_exp] < Time.now.tv_sec
    nil
  end

  def set_authenticated_user
    @authenticated_user = User.find_by(id: session[:user_id], banned: false)
  end

  def authenticate_session
    unless @authenticated_user
      reset_session
      return redirect_to login_url
    end

    session[:auth_exp] = Rails.configuration.x.session_expire_after.from_now.tv_sec
  end

  def set_theme
    if params.key?(:theme)
      cookies.permanent[Rails.configuration.x.theme_cookie] = {value: params[:theme], httponly: true}
      PerfCounter.hit("params.theme:#{params[:theme]}")
    end

    @theme = Theme.find_by(name: cookies[Rails.configuration.x.theme_cookie]) || Theme.default
    PerfCounter.hit("theme_class:#{@theme.css_class}")
  end

  def set_brand_path
    @brand_path = root_path
  end
end
