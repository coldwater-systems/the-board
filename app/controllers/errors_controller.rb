# frozen_string_literal: true

class ErrorsController < ApplicationController
  skip_before_action :authenticate_session

  # GET /404
  def not_found
    render status: :not_found
  end

  # GET /422
  def unacceptable
    render status: :unprocessable_entity
  end

  # GET /500
  def internal_error
    render status: :internal_server_error
  end
end
