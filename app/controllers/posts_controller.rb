# frozen_string_literal: true

class PostsController < ApplicationController
  before_action :set_post, only: [:show, :edit, :update, :destroy]

  # GET /
  def index
    @posts = Post.where.not(published_id: nil).order(created_at: :desc)
    if params.key?(:before_id)
      @before_id = params[:before_id]
      @posts = @posts.where(id: ...@before_id)
    end

    @posts = @posts.limit(5)
  end

  # GET /posts/1
  def show
    @post_comments = @post.comments.order(created_at: :desc)
    @new_post_comment = PostComment.new
  end

  # GET /posts/new
  def new
    @post_revision = PostRevision.new
    @form_url = posts_path
  end

  # GET /posts/1/edit
  def edit
    raise Forbidden if @post.owner != @authenticated_user

    @post_revision = @post.revisions.last
    @form_url = post_path(@post)
  end

  # POST /posts
  def create
    @post = Post.create!(owner: @authenticated_user)
    @post_revision = PostRevision.new(**post_revision_params, post: @post, author: @authenticated_user)
    if @post_revision.save
      if params[:button] == 'publish'
        @post.published = @post_revision
        @post.published_at ||= Time.now
        @post.save!
        redirect_to root_url
      else
        redirect_to @post
      end
    else
      render :new, status: :unprocessable_entity
    end
  end

  # PATCH /posts/1
  def update
    raise Forbidden if @post.owner != @authenticated_user

    @post_revision = PostRevision.new(**post_revision_params, post: @post, author: @authenticated_user)

    if @post_revision.save
      if params[:button] == 'publish'
        @post.published = @post_revision
        @post.published_at ||= Time.now
        @post.save!
        redirect_to root_url
      else
        redirect_to @post
      end
    else
      render :edit, status: :unprocessable_entity
    end
  end

  # DELETE /posts/1
  def destroy
    raise Forbidden if @post.owner != @authenticated_user

    @post.transaction do
      @post.published = nil
      @post.save!
      @post.revisions.each(&:destroy)
      @post.comments.each(&:destroy)
      @post.destroy
    end
    redirect_to root_url
  end

  private

  def set_post
    @post = Post.find(params[:id])
  end

  def post_revision_params
    params.require(:post_revision).permit(:body)
  end
end
