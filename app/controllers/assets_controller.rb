# frozen_string_literal: true

class AssetsController < ApplicationController
  skip_before_action :authenticate_session

  def favicon
    render 'application/favicon'
  end
end
