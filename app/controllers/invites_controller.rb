# frozen_string_literal: true

class InvitesController < ApplicationController
  # GET /invites is not routed (see SettingsController#show)

  # POST /invites
  def create
    Invite.create!(
      code:       generate_code,
      created_by: @authenticated_user,
      expires:    Rails.configuration.x.invite_expire_after.from_now
    )
    redirect_to settings_url
  end

  # DELETE /invites/:id
  def destroy
    @authenticated_user.created_invites.find(params[:id]).destroy
    redirect_to settings_url
  end

  private

  def generate_code = Base32.encode(SecureRandom.random_bytes(10))
end
