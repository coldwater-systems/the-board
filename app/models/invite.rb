# frozen_string_literal: true

class Invite < ApplicationRecord
  validates :code, :expires, presence: true
  belongs_to :created_by, class_name: 'User'
  belongs_to :claimed_by, class_name: 'User', optional: true

  def expires_str = format_date(expires)
  def claimed_at_str = claimed_by ? format_date(claimed_by.created_at_str) : 'Unclaimed'
end
