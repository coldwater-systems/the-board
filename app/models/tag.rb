# frozen_string_literal: true

class Tag < ApplicationRecord
  before_validation :set_slug
  validates :slug, uniqueness: true # TODO: not a good error message (slug is not user facing)

  def to_s = name
  def to_param = slug

  private

  def set_slug
    self.slug ||= name.parameterize
  end
end
