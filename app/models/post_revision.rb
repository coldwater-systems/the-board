# frozen_string_literal: true

class PostRevision < ApplicationRecord
  belongs_to :post
  belongs_to :author, class_name: 'User'
  has_rich_text :body
end
