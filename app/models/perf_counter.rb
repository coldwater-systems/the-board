# frozen_string_literal: true

class PerfCounter < ApplicationRecord
  class << self
    def [](name)
      find_by(name:)&.value
    end

    def []=(name, value)
      if (record = find_by(name:))
        record.value = value
        record.save
      else
        new(name:, value:).save
      end
    end

    def hit(name)
      transaction do
        if (record = find_by(name:))
          record.value += 1
          record.save
        else
          new(name:, value: 1).save
        end
      end
    end

    def respond_to_missing?(method, _include_all)
      method =~ /\Ahit_\w+\z/
    end

    def method_missing(method, *args)
      return super if method !~ /\Ahit_(\w+)\z/
      raise ArgumentError, "wrong number of arguments (given #{args.length}, expected 0)" unless args.empty?

      hit(Regexp.last_match(1))
    end
  end
end
