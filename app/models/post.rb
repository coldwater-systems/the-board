# frozen_string_literal: true

class Post < ApplicationRecord
  belongs_to :owner, class_name: 'User'
  belongs_to :published, class_name: 'PostRevision', optional: true
  has_many :revisions, class_name: 'PostRevision'
  has_many :comments, class_name: 'PostComment'

  def published_at_str = format_datetime(published_at)
end
