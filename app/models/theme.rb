# frozen_string_literal: true

class Theme
  include ActiveModel::Model

  def initialize(name, primary_hex, secondary_hex_or_interpolation, background_hex)
    @name = name
    @primary = ColorSpace::Srgb.from_hex(primary_hex).to_oklab
    @background = ColorSpace::Srgb.from_hex(background_hex).to_oklab
    @secondary = if secondary_hex_or_interpolation.is_a?(String)
                   ColorSpace::Srgb.from_hex(secondary_hex_or_interpolation).to_oklab
                 else
                   @background.interpolate(@primary, secondary_hex_or_interpolation)
                 end
  end
  attr_reader :name, :primary, :secondary, :background

  def css_class
    "theme-#{name}"
  end

  class << self
    DEFAULT_THEME = 'plain'
    THEMES = [
      Theme.new('plain',        '#000000', 0.3,       '#FFFFFF'),
      Theme.new('ink',          '#FFFFFF', 0.5,       '#000000'),
      Theme.new('sky',          '#dbd6d4', 0.3,       '#12293C'),
      Theme.new('moss',         '#dae5c7', 0.3,       '#394735'),
      Theme.new('mint',         '#CFFFEB', 0.3,       '#206A6A'),
      Theme.new('stone',        '#FFF9E9', 0.3,       '#4A4843'),
      Theme.new('peanut',       '#FFF1BD', 0.3,       '#593D31'),
      Theme.new('banana',       '#775D00', 0.3,       '#FEFFB8'),
      Theme.new('strawberry',   '#63272C', 0.3,       '#D8A2C2'),
      # Theme.new('marine',       '#FFF9E9', 0.3,       '#003676'),
      Theme.new('4am',          '#CA323B', 0.5,       '#000000'),
      Theme.new('hacker',       '#2BFF60', '#29533C', '#000000'),
      Theme.new('hacker-retro', '#f9801d', '#6d2f1b', '#000000'),
    ].freeze

    def all = THEMES
    def default = find_by!(name: DEFAULT_THEME)
    def find_by(name:) = by_name[name]
    def find_by!(name:) = by_name.fetch(name)

    private

    def by_name
      @by_name ||= THEMES.index_by(&:name)
    end
  end
end
