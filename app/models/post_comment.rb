# frozen_string_literal: true

class PostComment < ApplicationRecord
  belongs_to :post
  belongs_to :author, class_name: 'User'

  def to_partial_path = 'posts/comment'
  def to_s = text
end
