# frozen_string_literal: true

class ApplicationRecord < ActiveRecord::Base
  primary_abstract_class

  def created_at_str = format_datetime(created_at)
  def updated_at_str = format_datetime(updated_at)

  private

  def format_datetime(time)
    if time && time > 6.months.ago
      time.in_time_zone('America/Chicago').strftime('%b %-d · %-I:%M%P %Z')
    else
      format_date(time)
    end
  end

  def format_date(time)
    time.in_time_zone('America/Chicago').strftime('%b %-d, %Y') if time # rubocop:disable Style/SafeNavigation
  end
end
