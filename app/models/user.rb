# frozen_string_literal: true

class User < ApplicationRecord
  validates :username, :password_digest, presence: true
  validates :can_invite, :banned, inclusion: {in: [true, false]}
  validates :username, uniqueness: true
  validates :username, length: {minimum: 4, maximum: 20}
  validates :display_name, length: {minimum: 4, maximum: 64}
  has_secure_password
  validates :password, length: {minimum: 8, allow_nil: true}
  validate :password_is_not_weak

  belongs_to :invited_by, class_name: 'User', optional: true
  has_many :invited_users, class_name: 'User', foreign_key: :invited_by_id
  has_many :created_invites, class_name: 'Invite', foreign_key: :created_by_id
  has_one :claimed_invite, class_name: 'Invite', foreign_key: :claimed_by_id

  def self.find_and_authenticate(username, password)
    User.find_by(username:)&.authenticate(password)
  end

  def to_s = display_name
  def to_param = username

  def flags
    [
      ('is an operator' if moderator?),
      ('is a moderator' if moderator?),
      ('can send invites' if can_invite?),
      ('is banned' if banned?),
    ].compact.to_human('and')
  end

  def can_edit?(post)
    self == post.author
  end

  def can_delete?(post)
    post.replies.empty? && (moderator? || self == post.author)
  end

  private

  def password_is_not_weak
    errors.add(:password, I18n.t('errors.messages.weak')) if WeakPasswords.include?(password)
  end
end
