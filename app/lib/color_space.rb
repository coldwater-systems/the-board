# frozen_string_literal: true

# Primary References:
# - https://bottosson.github.io/posts/oklab/
# - https://bottosson.github.io/posts/colorwrong/
# - https://www.w3.org/TR/css-color-4/#ok-lab
# - TODO: https://www.w3.org/WAI/WCAG21/Understanding/contrast-minimum.html
# Secondary References:
# - https://oklch.com/
# - https://ajalt.github.io/colormath/converter/
module ColorSpace
  # rubocop:disable Naming/MethodParameterName
  class Oklch
    def initialize(l, c, h)
      @l = Float(l)
      @c = Float(c)
      @h = Float(h)
    end
    attr_accessor :l, :c, :h

    def to_oklab
      ColorSpace::Oklab.new(@l, @c * Math.cos(@h / 180 * Math::PI), @c * Math.sin(@h / 180 * Math::PI))
    end

    delegate :to_linear_srgb, to: :to_oklab
    def to_srgb = to_oklab.to_linear_srgb.to_srgb
  end

  class Oklab
    def initialize(l, a, b)
      @l = Float(l)
      @a = Float(a)
      @b = Float(b)
    end
    attr_accessor :l, :a, :b

    def to_linear_srgb
      lng_ = @l + 0.3963377774 * @a + 0.2158037573 * @b
      med_ = @l - 0.1055613458 * @a - 0.0638541728 * @b
      srt_ = @l - 0.0894841775 * @a - 1.2914855480 * @b

      lng = lng_ * lng_ * lng_
      med = med_ * med_ * med_
      srt = srt_ * srt_ * srt_

      ColorSpace::LinearSrgb.new(
        +4.0767416621 * lng - 3.3077115913 * med + 0.2309699292 * srt,
        -1.2684380046 * lng + 2.6097574011 * med - 0.3413193965 * srt,
        -0.0041960863 * lng - 0.7034186147 * med + 1.7076147010 * srt
      )
    end

    delegate :to_srgb, to: :to_linear_srgb
    def to_hex = to_linear_srgb.to_srgb.to_hex

    def to_s
      format('oklab(%6.2<l>f%% %7.4<a>f %7.4<b>f)', l: @l * 100, a: @a, b: @b)
    end

    def interpolate(other, amount)
      ColorSpace::Oklab.new(
        @l * (1 - amount) + other.l * amount,
        @a * (1 - amount) + other.a * amount,
        @b * (1 - amount) + other.b * amount
      )
    end
  end

  class LinearSrgb
    def initialize(r, g, b)
      @r = Float(r)
      @g = Float(g)
      @b = Float(b)
    end
    attr_accessor :r, :g, :b

    def to_srgb
      ColorSpace::Srgb.new(
        srgb_transform(@r),
        srgb_transform(@g),
        srgb_transform(@b)
      )
    end

    def to_oklab
      lng = 0.4122214708 * @r + 0.5363325363 * @g + 0.0514459929 * @b
      med = 0.2119034982 * @r + 0.6806995451 * @g + 0.1073969566 * @b
      srt = 0.0883024619 * @r + 0.2817188376 * @g + 0.6299787005 * @b

      lng_ = lng**(1.0 / 3)
      med_ = med**(1.0 / 3)
      srt_ = srt**(1.0 / 3)

      ColorSpace::Oklab.new(
        0.2104542553 * lng_ + 0.7936177850 * med_ - 0.0040720468 * srt_,
        1.9779984951 * lng_ - 2.4285922050 * med_ + 0.4505937099 * srt_,
        0.0259040371 * lng_ + 0.7827717662 * med_ - 0.8086757660 * srt_
      )
    end

    private

    def srgb_transform(x)
      if x >= 0.0031308
        1.055 * x**(1 / 2.4) - 0.055
      else
        12.92 * x
      end
    end
  end

  class Srgb
    def self.from_hex(str)
      raise ArgumentError, "#{str.inspect} not a 6 digit hex string" if str !~ /\A#?[0-9a-fA-F]{6}\z/

      str = str[1..] if str.start_with? '#'

      rgb = [str].pack('H*').unpack('C*').map {|x| x.fdiv(255) }
      new(*rgb)
    end

    def initialize(r, g, b)
      @r = Float(r)
      @g = Float(g)
      @b = Float(b)
    end
    attr_accessor :r, :g, :b

    def to_linear_srgb
      ColorSpace::LinearSrgb.new(
        srgb_inv_transform(@r),
        srgb_inv_transform(@g),
        srgb_inv_transform(@b)
      )
    end

    def to_hex
      # XXX: naive clipping (see https://bottosson.github.io/posts/gamutclipping/ for how to do this correctly)
      format('#%02<r>X%02<g>X%02<b>X',
             r: (@r * 255).round.clamp(0, 255),
             g: (@g * 255).round.clamp(0, 255),
             b: (@b * 255).round.clamp(0, 255))
    end

    delegate :to_oklab, to: :to_linear_srgb

    private

    def srgb_inv_transform(x)
      if x >= 0.04045
        ((x + 0.055) / 1.055)**2.4
      else
        x / 12.92
      end
    end
  end
  # rubocop:enable Naming/MethodParameterName
end
