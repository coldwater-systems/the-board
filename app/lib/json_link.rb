# frozen_string_literal: true

class JsonLink
  def self.to(href, **)
    new(href:, **) if href
  end

  def initialize(**params)
    @params = params
  end

  def as_json(_options = {})
    @params
  end
end
