# frozen_string_literal: true

class WeakPasswords
  class << self
    def include?(unecrypted_password)
      SET.include?(unecrypted_password)
    end

    def read_list(filename = File.expand_path('weak_passwords.txt.xz', __dir__))
      set = Set.new
      XZ::StreamReader.open(filename) do |file|
        # Have to slurp the entire file because the xz gem performs poorly
        # when read line-by-line
        strio = StringIO.new(file.read.encode!('UTF-8'))
        while (line = strio.gets&.chomp)
          set << line
        end
      end
      set
    end
  end

  SET = WeakPasswords.read_list.freeze
end
