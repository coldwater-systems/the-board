# frozen_string_literal: true

require_relative '../../lib/system'

# Where possible, this uses the vocabulary seen in https://semver.org
#
# release_info.json should contain the following info, filled in by CI:
# {
#   "prerelease": e.g. "alpha", or none at all for a full release
#   "build": any info that comes after the version number (separated by a '+')
#   "commit_sha": e.g. $CI_COMMIT_SHORT_SHA in GitLab
#   "source_code_url": e.g. $CI_PROJECT_URL/-/tree/$CI_COMMIT_SHORT_SHA in GitLab
#   "issues_url": e.g. $CI_PROJECT_URL/-/issues in GitLab
# }
#
module Manifest
  VERSION_CORE = '0.2.11' # MAJOR.MINOR.PATCH
  RELEASE_INFO_PATH = Rails.root / 'config/release_info.json'

  class << self
    def version
      [
        VERSION_CORE,
        ("-#{release_info[:prerelease]}" if release_info[:prerelease]),
        ("+#{release_info[:build]}" if release_info[:build]),
      ].compact.join
    end

    def commit_sha = release_info[:commit_sha]
    def source_code_url = release_info[:source_code_url]
    def issues_url = release_info[:issues_url]

    private

    def release_info
      @release_info ||= begin
        JSON.parse(RELEASE_INFO_PATH.read, symbolize_names: true).freeze
      rescue Errno::ENOENT
        default_release_info.freeze
      end
    end

    def default_release_info
      source_code_url = System.capture(%w[git remote get-url origin])
      source_code_url.sub!(/\Agit@([^:]+):/, 'https://\1/')
      source_code_url.sub!(/\.git\z/, '')

      {
        prerelease:      'HEAD',
        source_code_url:,
        issues_url:      "#{source_code_url}/-/issues",
      }
    end
  end
end
