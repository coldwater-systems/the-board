# frozen_string_literal: true

require 'shellwords'

module System
  class << self
    def run?(*, **)
      argv0, *argv = coerce_argv(*)

      warn [argv0, *argv].shelljoin
      system([argv0, argv0], *argv, **)
    end

    def run(*, **)
      argv0, *argv = coerce_argv(*)

      warn [argv0, *argv].shelljoin
      system([argv0, argv0], *argv, exception: true, **)
    end

    def capture(args, **)
      argv0, *argv = coerce_argv(*args)

      result = IO.popen([argv0, *argv], **, &:read)
      raise "#{argv0} failed (#{Process.last_status})" unless Process.last_status.success?

      result.chomp
    end

    private

    def coerce_argv(*args)
      args = Array(args.first) if args.length == 1
      raise ArgumentError, 'Must specify one or more argument' if args.empty?

      args
    end
  end
end
