# frozen_string_literal: true

require 'rails_helper'

RSpec.describe '/session' do
  describe 'GET /login' do
    it 'renders a successful response' do
      get login_url
      expect(response).to be_successful
    end
  end
end
