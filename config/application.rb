# frozen_string_literal: true

require_relative 'boot'

require 'rails/all'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module TheBoard
  class Application < Rails::Application
    # Initialize configuration defaults for originally generated Rails version.
    config.load_defaults 7.0

    config.exceptions_app = routes
    config.action_dispatch.rescue_responses['ArgumentError'] = :bad_request
    config.action_dispatch.rescue_responses['ApplicationController::Forbidden'] = :forbidden
    config.action_dispatch.rescue_responses['ApplicationController::ForbiddenAndInvisible'] = :not_found

    # TODO: determine if these should be added
    # config.action_dispatch.cookies_serializer = :json # Valid options are :json, :marshal, and :hybrid.
    # config.session_store :cookie_store, expire_after: 7.days
    config.x.session_expire_after = 7.days
    config.x.invite_expire_after = 7.days
    config.x.theme_cookie = 'the_board_theme'
  end
end
