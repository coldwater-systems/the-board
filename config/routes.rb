# frozen_string_literal: true

Rails.application.routes.draw do
  get    '/404',                       to: 'errors#not_found'
  get    '/422',                       to: 'errors#unacceptable'
  get    '/500',                       to: 'errors#internal_error'
  get    '/favicon.ico',               to: redirect('/favicon.svg')
  get    '/favicon.svg',               to: 'assets#favicon'

  mount TheBoard::ApiV1 => '/api/v1'

  get    '/invite/:code',              to: 'profile#accept_invite', as: 'accept_invite'
  get    '/profile/new',               to: 'profile#new', as: 'new_profile'
  get    '/profile',                   to: 'profile#redirect_to_user'
  post   '/profile',                   to: 'profile#create'
  get    '/profile/edit',              to: 'profile#edit'
  patch  '/profile',                   to: 'profile#update'
  get    '/profile/username',          to: redirect('/settings')
  patch  '/profile/username',          to: 'profile#update_username'

  get    '/login',                     to: 'session#login'
  post   '/login',                     to: 'session#check_login'
  post   '/logout',                    to: 'session#logout'
  get    '/password',                  to: redirect('/settings')
  patch  '/password',                  to: 'session#update_password'

  get    '/settings',                  to: 'settings#show'

  get    '/invites',                   to: redirect('/settings')
  post   '/invites',                   to: 'invites#create'
  get    '/invites/:id',               to: redirect('/settings'), as: 'invite'
  delete '/invites/:id',               to: 'invites#destroy'

  get    '/users/:username',           to: 'users#show', as: 'user'
  post   '/users/:username/ban',       to: 'users#ban', as: 'ban_user'
  post   '/users/:username/unban',     to: 'users#unban', as: 'unban_user'

  root                                     'posts#index'
  get    '/posts',                     to: redirect('/')
  get    '/posts/new',                 to: 'posts#new', as: 'new_post'
  post   '/posts',                     to: 'posts#create'
  get    '/posts/:id',                 to: 'posts#show', as: 'post'
  get    '/posts/:id/edit',            to: 'posts#edit', as: 'edit_post'
  patch  '/posts/:id',                 to: 'posts#update'
  delete '/posts/:id',                 to: 'posts#destroy'

  get    '/posts/:id/comments',        to: 'post_comments#redirect_to_post', as: 'post_comments'
  post   '/posts/:id/comments',        to: 'post_comments#create'

  get    '/tags',                      to: 'tags#index'
  get    '/tags/new',                  to: 'tags#new', as: 'new_tag'
  post   '/tags',                      to: 'tags#create'
  get    '/tags/:slug',                to: redirect('/tags'), as: 'tag'
  delete '/tags/:slug',                to: 'tags#destroy'
end
