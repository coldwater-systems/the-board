# frozen_string_literal: true

class ApplicationFormBuilder < ActionView::Helpers::FormBuilder
  def errors_for(*)
    errors = object.errors.where(*)
    return nil if errors.empty?

    messages = [errors.first.full_message]
    messages.concat(errors[1..].map(&:message))

    @template.tag.div(class: 'errors') do
      "#{messages.to_human('and').capitalize}."
    end
  end
end

ActionView::Base.default_form_builder = ApplicationFormBuilder
