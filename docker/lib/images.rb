# frozen_string_literal: true

require_relative '../../lib/system'
require_relative 'fingerprint'

module Images
  USE_RUBY_VERSION = File.read(File.expand_path('../../.ruby-version', __dir__))[/\Aruby-([\d.]+)\Z/, 1].freeze
  REGISTRY_PREFIX = 'registry.gitlab.com/coldwater-systems/the-board'
  REF_NAME = ENV.fetch('CI_COMMIT_REF_NAME') { System.capture(%w[git symbolic-ref --short HEAD]) }
  GEMS_FINGERPRINT = Fingerprint.of('Gemfile', 'Gemfile.lock', 'docker/the-board-gems/Dockerfile', at: '../..')

  RUBY_TAG = "ruby:#{USE_RUBY_VERSION}-alpine".freeze
  THE_BOARD_GEMS_TAG = "#{REGISTRY_PREFIX}/the-board-gems:#{GEMS_FINGERPRINT}".freeze
  THE_BOARD_TAG = "#{REGISTRY_PREFIX}/the-board:#{REF_NAME}".freeze
end
