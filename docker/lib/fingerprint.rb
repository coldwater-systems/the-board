# frozen_string_literal: true

require 'openssl'

module Fingerprint
  def self.of(*paths, at: '.')
    Dir.chdir(at) do
      b2 = OpenSSL::Digest.new('BLAKE2b512')

      digests = paths.map do |path|
        "#{path}  #{b2.hexdigest(File.read(path))}"
      end
      b2.hexdigest(digests.join("\0"))[0...32]
    end
  end
end
