
![The Board Logo](/docs/brand_logo.svg)
---
The Board is an experimental lightweight message board designed to fill the same
niche as the traditional forum or BBS. It is currently invite only and features
rich text and image posting with plain text comments.

![A post on The Board](/docs/tb-home.png)
### Current Features
- Rich text posting with images, drafts and editing
- Plain text post comments
- User invite system
- Myriad of UI color-scheme options
- Mobile and desktop friendly

### Setup
- Make sure you have ruby 3.2 installed and run `bin/setup`. 
- If you would like to generate a dummy admin account for development, you can
seed the database using `rails db:seed`. (The password and username will be
`admin`)
- To run the application simply run `rails s`.

### The Future
#### 0.3
The Board is currently undergoing it's 3rd major alpha rewrite to accommodate
some bigger design changes that we would like to implement. 
- Since the beginning of the second rewrite we've wanted to add tagging as the
	primary means of organization; 0.3 will realize this functionality. 
- We've also discovered that some posts look really good when using certain
	themes, and decided to add theme tags, and theme their respective indices
- Migration away from ActionText as the main text editor in favor of
  [TipTap](https://tiptap.dev). ActionText was a bit difficult to modify and
  extend for our purposes, and TipTap's ground up approach has already proven
  more suitable in experimentation.
- Comments will no longer be plain text, and will instead be full featured posts
  tagged as replies. This reduces complexity by removing an entire concept from
  the back end.
- Adding [PaperTrail](https://github.com/paper-trail-gem/paper_trail) to support
  more robust tracking (useful for finding out who added which tags, potentially
  rolling back edits, etc.)
- Testing [ViewComponent](https://viewcomponent.org) to aid view refactoring
- Testing [Lookbook](https://lookbook.build)
- Migrating from [RemixIcon](https://remixicon.com) to [Phosphor
  Icons](https://phosphoricons.com). RemixIcon was causing alignment issues, and
  [Phosphor's ruby gem](https://github.com/maful/ruby-phosphor-icons) integrates
  more seamlessly with Rails.
#### Beyond 0.3
- Multi-tag filters, to facilitate feed customization for a user
- Better out of the box customization experience for people running their own
  instance
- Full test coverage
- Full featured moderation tools
